import sjcl from 'sjcl'

const randomString = (length) => {
  return [...Array(length)].map(i=>(~~(Math.random()*36)).toString(36)).join('');
}

const popupDimensions = (w, h) => {
  // Fixes dual-screen position                             Most browsers      Firefox
  const dualScreenLeft = window.screenLeft !==  undefined ? window.screenLeft : window.screenX;
  const dualScreenTop = window.screenTop !==  undefined   ? window.screenTop  : window.screenY;

  const width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
  const height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

  const systemZoom = width / window.screen.availWidth;
  const left = (width - w) / 2 / systemZoom + dualScreenLeft;
  const top = (height - h) / 2 / systemZoom + dualScreenTop;

  return `scrollbars=yes,width=${w / systemZoom},height=${h / systemZoom},top=${top},left=${left},modal=yes,alwaysRaised=yes`;
}

export const authenticateWithCodeChallenge = ({ authProviderUrl, popupTitle }) => {
  const codeVerifier = randomString(64);
  const codeVerifier64 = btoa(codeVerifier);

  const myBitArray = sjcl.hash.sha256.hash(codeVerifier64)
  const codeChallenge = btoa(sjcl.codec.hex.fromBits(myBitArray));

  const getToken = (code, cb) => {
    const xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function() {
      if (xhr.readyState === XMLHttpRequest.DONE) {
        if (xhr.status == 200) {
          cb(JSON.parse(xhr.responseText));
        } else {
          cb(null);
        }
      }
    };
    xhr.open("GET", `${authProviderUrl}/protocol/openid-connect/token?grant_type=authorization_code&code=${code}&code_verifier=${codeVerifier64}`, true);
    xhr.send();
  }

  const authWindow = window.open(
    `${authProviderUrl}/protocol/openid-connect/auth?response_type=code&code_challenge=${codeChallenge}&code_challenge_method=S256&redirect_uri=http://localhost:5000`,
    popupTitle || 'Authentication',
    popupDimensions(400, 400)
  );

  const checkState = (onSuccess, onError) => {
    try {
      const queryString = authWindow.location.search;
      const urlParams = new URLSearchParams(queryString);
      if (urlParams.get('authorization_code')) {
        getToken(urlParams.get('authorization_code'), onSuccess);
        authWindow.close();
      }
    } catch(error) {
      onError(error);
    }
    if (!authWindow.closed) {
      setTimeout(() => checkState(onSuccess, onError), 2000);
    }
  }

  return new Promise((resolve, reject) => {
    checkState(resolve, reject);
  });
}
